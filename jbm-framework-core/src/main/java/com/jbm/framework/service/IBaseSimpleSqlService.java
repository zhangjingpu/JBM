package com.jbm.framework.service;

import com.jbm.framework.metadata.usage.bean.BaseEntity;

public interface IBaseSimpleSqlService<Entity extends BaseEntity> extends IBaseSqlService<Entity, Long> {

}

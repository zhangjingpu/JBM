package com.jbm.framework.handler;

/**
 * 基于Mybatis的基础DAO接口
 * 
 * @author wesley
 * 
 * @param <Entity>
 *            业务实体类型
 */
public interface IBaseHandlerService<Entity> extends IMicroHandlerService {

}

package com.jbm.shop.server.bean;

import java.util.Map;

public class WeiDianResuleBean {

	private Map<String, Object> result;

	private Map<String, Object> status;

	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}

	public Map<String, Object> getStatus() {
		return status;
	}

	public void setStatus(Map<String, Object> status) {
		this.status = status;
	}

}

package com.jbm.ev.program.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.jbm.sample.entity.AggregateData;

public interface DataMapper extends BaseMapper<AggregateData> {

}

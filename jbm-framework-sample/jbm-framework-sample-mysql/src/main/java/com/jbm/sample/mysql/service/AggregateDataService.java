package com.jbm.sample.mysql.service;

import com.jbm.framework.service.IAdvSqlService;
import com.jbm.sample.entity.AggregateData;

/**
 * @author jim.xie
 * @version version1.0
 * @Description: TODO
 * @date 2017年5月10日 下午2:18:32
 */
public interface AggregateDataService extends IAdvSqlService<AggregateData, Long> {

}

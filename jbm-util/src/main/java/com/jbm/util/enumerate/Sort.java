package com.jbm.util.enumerate;

/**
 * 排序类型枚举
 */
public enum Sort {

	ASC, DESC

}

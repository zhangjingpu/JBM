package com.jbm.util.inf;

/**
 * 
 * 追加接口
 * 
 * @author wesley
 *
 * @param <T>
 * @param <V>
 */
public interface IAppend<T, V> {

	T append(V value);

}

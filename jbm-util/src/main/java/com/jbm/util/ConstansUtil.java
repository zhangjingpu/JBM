package com.jbm.util;

public class ConstansUtil {

	public static final String FAIL = "失败";
	
	public static final String SUCCESS = "成功";
	
	/**
	 * 电站id为空
	 */
	public static final String SITE_IS_NULL = "电站id为空";
	
	/**
	 * 设备类型为空
	 */
	public static final String TYPE_IS_NULL = "设备类型为空";
	
	/**
	 * 设备SN号为空
	 */
	public static final String SN_IS_NULL = "设备SN号为空";
	
	/**
	 * 设备CODE为空
	 */
	public static final String CODE_IS_NULL = "设备CODE为空";
	
	/**
	 * 错误的设备CODE
	 */
	public static final String CODE_IS_ERROR = "错误的设备CODE";
	
	/**
	 * 设备信息不存在
	 */
	public static final String DEVICE_NOT = "设备信息不存在";
	
	/**
	 * 父设备信息不存在
	 */
	public static final String DEVICE_PARENT_NOT = "父设备信息不存在";
	
	/**
	 * 组织基本信息为空
	 */
	public static final String ORG_MAIN_NOT = "组织基本信息为空";
	
	/**
	 * 父组织基本信息为空
	 */
	public static final String PARENTID_ERROR = "父组织基本信息为空";
	
	/**
	 * 新增组织基本信息失败
	 */
	public static final String ORG_MAIN_ADD_ERROR = "新增组织基本信息失败";
	
	/**
	 * 更新组织基本信息失败
	 */
	public static final String ORG_MAIN_UPDATE_ERROR = "更新组织基本信息失败";
	
	/**
	 * 新增组织关系失败
	 */
	public static final String ORG_RELATION_ERROR = "新增组织关系失败";
	
	/**
	 * 更新组织关系失败
	 */
	public static final String ORG_RELATION_UPDATE_ERROR = "更新组织关系失败";
	
	/**
	 * 新增组织财务信息失败
	 */
	public static final String ORG_FINANCE_ERROR = "新增组织财务信息失败";
	
	/**
	 * 更新组织财务信息失败
	 */
	public static final String ORG_FINANCE_UPDATE_ERROR = "更新组织财务信息失败";
	
	/**
	 * 新增组织法律信息失败
	 */
	public static final String ORG_LEGAL_ERROR = "新增组织法律信息失败";
	
	/**
	 * 更新组织法律信息失败
	 */
	public static final String ORG_LEGAL_UPDATE_ERROR = "更新组织法律信息失败";

    /**
     * 下划线
     */
    public static final String UNDER_LINE = "_";

    /**
     * 逗号
     */
    public static final String sep_1 = ",";

    /**
     * 空
     */
    public static final String sep_2 = "";

    /**
     * 换行
     */
    public static final String sep_3 = "\n";

    /**
     * 换行空格
     */
    public static final String sep_4 = "\n\r";

    /**
     * 单位转换数据cache
     */
    public static final String UNIT_LIST_CACHE = "unit_list_cache";

    /**
     * 站点id
     */
    public static final String SYS_SITEID = "siteId";

    /**
     * 站点id site_id
     */
    public static final String SYS_SITEID_1 = "site_id";

    /**
     * 开始时间 beginTime
     */
    public static final String BEGIN_TIME = "beginTime";

    /**
     * 结束时间endTime
     */
    public static final String END_TIME = "endTime";

    /**
     * 时间time
     */
    public static final String TIME = "time";

    /**
     * 新增时间
     */
    public static final String ADD_TIME = "addTime";

    /**
     * 类型type
     */
    public static final String TYPE = "type";

    /**
     * 数据列表list
     */
    public static final String LIST = "list";

    /**
     * 最大值
     */
    public static final String MAX_DATA = "maxData";

    /**
     * energyData
     */
    public static final String ENERGR_DATA = "energyData";

    /**
     * maxEnergy
     */
    public static final String MAX_ENERGR = "maxEnergy";

    /**
     * 频率perHourFrequency
     */
    public static final String PER_HOUR = "perHourFrequency";

    /**
     * 设备类型deviceType
     */
    public static final String DEVICE_TYPE = "deviceType";

    /**
     * 设备SN码serialNumber
     */
    public static final String SERIAL_NUM = "serialNumber";

    /**
     * 设备SN码serialNumbers
     */
    public static final String SERIAL_NUMS = "serialNumbers";

    /**
     * 设备SN码serial_number
     */
    public static final String SERIAL_NUM_1 = "serial_number";

    /**
     * 查询key
     */
    public static final String KEYS = "keys";

    /**
     * 开始start
     */
    public static final String START = "start";

    /**
     * 每页数limit
     */
    public static final String LIMIT = "limit";

    /**
     * 排序 sort
     */
    public static final String SORT = "sort";

    /**
     * 排序顺序 dir
     */
    public static final String DIR = "dir";

    /**
     * 倒序desc
     */
    public static final String DESC = "desc";

    /**
     * 总发电量 e_total
     */
    public static final String ETOTAL = "e_total";

    /**
     * 计算发电量配置类型
     */
    public static final String ENERGY = "energy";

    /**
     * 理想发电量
     */
    public static final String DEF_IDEAL_ENERGY = "defIdealEnergy";

    /**
     * 理想发电量
     */
    public static final String IDEAL_ENERGY = "idealEnergy";

    /**
     * 电表发电量
     */
    public static final String METER_ENERGY = "meterEnergy";

    /**
     * 日发电量数据统计
     */
    public static final String DAYTOTAL = "dayTotal";

    /**
     * 电流
     */
    public static final String CURRENT = "current";

    /**
     * 电压
     */
    public static final String VOLATAGE = "voltage";

    /**
     * 功率
     */
    public static final String POWER = "power";

    /**
     * 直流功率
     */
    public static final String DC_POWER = "dcPow";

    /**
     * 电表正向有功电度
     */
    public static final String CUM_ENERGY = "cumulative_energy";

    /**
     * true
     */
    public static final String TRUE = "true";

    /**
     * 逆变器设备
     */
    public static final int INV_TYPE = 1;

    /**
     * 汇流箱设备
     */
    public static final int BOX_TYPE = 2;

    /**
     * 环境设备
     */
    public static final int ENV_TYPE = 5;

    /**
     * 电表设备
     */
    public static final int EM_TYPE = 6;

    /**
     * 汇集线
     */
    public static final int LINE_TYPE = 9;
}

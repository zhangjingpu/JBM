package com.jbm.framework.service.support;

public interface ISessionSupport {

	public String getSessionId();
}

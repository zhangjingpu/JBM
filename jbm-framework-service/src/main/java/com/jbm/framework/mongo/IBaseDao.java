package com.jbm.framework.mongo;

import java.io.Serializable;

/**
 * 
 * 基础数据库处理层的接口
 * 
 * @author Wesley
 * 
 * @param <Entity>
 * @param <PK>
 */
public interface IBaseDao<Entity extends Serializable, PK extends Serializable> {

}
